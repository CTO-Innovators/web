﻿namespace EzClinicWeb.Controllers
{
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Welcome()
        {
            return View();
        }
    }
}