﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EzClinicWeb.Models
{
    public class PatientDetailsForResponse
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int64 MobileNo { get; set; }
        public string Message { get; set; }
    }
}