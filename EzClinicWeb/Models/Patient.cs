﻿namespace EzClinicWeb.Models
{
    using System;

    public class Patient
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public Int64 MobileNo { get; set; }
        public string Email { get; set; }
        public string NfcReference { get; set; }
        public string CreditCardType { get; set; }
        public Int64 CreditCardNo { get; set; }
        public int CreditCardExpMonth { get; set; }
        public int CreditCardExpYear { get; set; }
        public string NameOnCard { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int Password { get; set; }
    }
}